# Description
This Dockerfile generates an image containing cmake.

# Usage
- `docker build -t cmake .`
- `docker run --volume "$PWD:/code" cmake`
<br /> OR
- `docker run --volume "$PWD:/code" registry.gitlab.com/francoissevestre/dockerfiles/cmake` 

# Requirements
- $PWD must contain a CMakeLists.txt file.
